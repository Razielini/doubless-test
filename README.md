# DoubleSS TEST #

##Consigna
Hacer un diseño responsive, totalmente fluido de un grid.


##Original
El archivo html/src/original.html tiene un ejemplo apegado al original, que es un grid de 4 x 2.

##Full Fluid
El archivo html/src/full-fluid.html tiene un ejemplo menos apegado al original, con un grid fluido, donde segun el tamaño de la pantalla puede tener tantos elementos como sea posible. Por ejemplo en un monitor de 1920px puede llegar ha tener hasta 6 elementos por fila.

##API
Un ejemplo de tienda donde compras pokemon, se uso JQuery y solo puedes agregar pokemons, no puedes quitarlos una vez agregados.
Cuenta los items en la cesta y el total de tu compra.